class Campaign < ApplicationRecord
  belongs_to :user
  has_one_attached :image

  has_many :likes, dependent: :delete_all

  validates_presence_of :title, :description

  before_update :is_blocked

  private

  # verifica se a campanha está bloqueada e aborta o update da Campaing
  def is_blocked
    campaing = Campaign.find(self.id)
    if campaing.blocked
      throw(:abort)
    end
  end

end
