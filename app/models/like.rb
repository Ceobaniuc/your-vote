class Like < ApplicationRecord
  belongs_to :campaign
  belongs_to :user

  before_update :is_blocked

  private

  # verifica se a campanha está bloqueada e aborta o update da Campaing
  def is_blocked
    campaing = Campaign.find(self.id)
    if campaing.blocked
      throw(:abort)
    end
  end
end
